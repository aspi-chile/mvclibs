<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - type()
* - largoMin()
* - largoMax()
* - enteroPositivo()
* - entero()
* - enteroMayorIgualQue()
* - enteroMenorIgualQue()
* - nombre()
* - correo()
* - coincidencia()
* - rut()
* - fecha()
* - fechaY_mm_dd()
* Classes list:
* - Check
*/
class Check
  {
    function __construct()
      {;
      }
    public function type($data, $tipo)
      {
        if (gettype($data) != $tipo) return "No es el tipo de dato adecuado";
        return false;
      }
    public function largoMin($data, $min)
      {
        if (strlen($data) < $min)
          {
            return "debe ser tener un largo mayor a $min";
          }
        return false;
      }
    public function largoMax($data, $max)
      {
        if (strlen($data) > $max)
          {
            return "debe ser tener un largo menor a $max";
          }
        return false;
      }
    public function enteroPositivo($data, $arg)
      {
        if ((is_numeric($data) || ctype_digit($data)) && ( int )$data >= 0)
          {
            return false;
          }
        return "debe ingresar un numero entero positivo";
      }
    public function entero($data, $arg)
      {
        if ((is_numeric($data)))
          {
            return false;
          }
        return "debe ingresar un numero entero";
      }
    public function enteroMayorIgualQue($data, $arg)
      {
        if (is_numeric($data) && (((int)$data) >= $arg))
          {
            return false;
          }
        return "debe ingresar un numero mayor o igual a " . $arg;
      }
    public function enteroMenorIgualQue($data, $arg)
      {
        if (is_numeric($data) && (((int)$data) <= $arg))
          {
            return false;
          }
        return "debe ingresar un numero menor o igual a " . $arg;
      }
    public function nombre($data, $arg)
      {
        $data = str_replace(' ', '', $data);
        /*if(ctype_alpha($data)){
            return false;
        }*/
        $reg = "#[^\p{L}\s-]#u";
        $count = preg_match($reg, $data);
        if ($count == 0) return false;
        return "solo se aceptan caracteres alfabeticos";
      }
    public function correo($data, $arg)
      {
        if (!filter_var($data, FILTER_VALIDATE_EMAIL))
          {
            return "formato invalido";
          }
        return false;
      }
    public function coincidencia($data, $arg)
      {
        if ($data != $arg)
          {
            return "No hay coincidencia";
          }
        return false;
      }
    public function rut($data, $arg)
      {
        if ((!$data) or (is_array($data))) return false; /* Hace falta el rut */
        if (!$data = preg_replace('|[^0-9kK]|i', '', $data)) return false; /* Era código basura */
        if (!((strlen($data) == 8) or (strlen($data) == 9))) return false; /* La cantidad de carácteres no es válida. */
        $v = strtoupper(substr($data, -1));
        if (!$data = substr($data, 0, -1)) return false;
        if (!((int)$data > 0)) return false; /* No es un valor numérico */
        $x = 2;
        $s = 0;
        for ($i = (strlen($data) - 1);$i >= 0;$i--)
          {
            if ($x > 7) $x = 2;
            $s += ($data[$i] * $x);
            $x++;
          }
        $dv = 11 - ($s % 11);
        if ($dv == 10) $dv = 'K';
        if ($dv == 11) $dv = '0';
        if ($dv == $v) return false;
        return "invalido";
      }
    public function fecha($data, $arg)
      {
        $fechaHoraTemporal = str_replace("/", "-", $data);
        $fechaHoraTemporal = explode(' ', $fechaHoraTemporal);
        if (count($fechaHoraTemporal) != 2) return 'Formato Invalido debe ser Y-mm-dd H:i:s';
        $fechaTemporal = explode('-', $fechaHoraTemporal[0]);
        $horaTemporal = explode(':', $fechaHoraTemporal[1]);
        if (count($horaTemporal) != 3) return 'Formato Invalido debe ser Y-mm-dd H:i:s';
        if (count($fechaTemporal) != 3) return 'Formato Invalido debe ser Y-mm-dd H:i:s';
        if ((((int)($horaTemporal[0])) >= 24) && ((int)($horaTemporal[0]) < 0)) return '1Formato Invalido debe ser Y-mm-dd H:i:s';
        else if ((((int)($horaTemporal[1])) >= 60) && ((int)($horaTemporal[1]) < 0)) return '2Formato Invalido debe ser Y-mm-dd H:i:s';
        else if ((((int)($horaTemporal[2])) >= 60) && ((int)($horaTemporal[2] < 0))) return '3Formato Invalido debe ser Y-mm-dd H:i:s';
        if (!checkdate($fechaTemporal[1], $fechaTemporal[2], $fechaTemporal[0])) return '4Formato Invalido debe ser Y-mm-dd H:i:s';
        else return false;
      }
    public function fechaY_mm_dd($data, $arg)
      {
        $fechaHoraTemporal = str_replace("/", "-", $data);
        $fechaTemporal = explode('-', $fechaHoraTemporal);
        if (count($fechaTemporal) != 3) return 'Formato Invalido debe ser Y-mm-dd';
        if (!checkdate($fechaTemporal[1], $fechaTemporal[2], $fechaTemporal[0])) return '4Formato Invalido debe ser Y-mm-dd';
        else return false;
      }
  }
?>
