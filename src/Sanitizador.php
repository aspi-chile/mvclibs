<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - email()
* - texto()
* - nombre()
* - rut()
* Classes list:
* - Sanitizador
*/
class Sanitizador
  {
    function __construct()
      {;
      }
    public function email($data)
      {
        return filter_var($data, FILTER_SANITIZE_EMAIL);
      }
    public function texto($data)
      {
        $value = strip_tags($data);
        return trim($value);
      }
    public function nombre($data)
      {
        if (strlen($data) > 255) $data = substr($data, 0, 255);
        $data = strip_tags($data);
        return $data;
      }
    public function rut($data)
      {
        $data = str_replace(".", "", $data);
        $data = str_replace(" ", "", $data);
        return $data;
      }
  }

