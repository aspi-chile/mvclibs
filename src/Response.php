<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - __set()
* - __get()
* - __toString()
* - emitirJson()
* Classes list:
* - Response
*/
class Response
{
    public $error;
    public $data;
    function __construct()
    {
        $this->error = new ErrorResponse();
        $this->data = array();
    }
    public function __set($var, $valor)
    {
        if (property_exists('Response', $var))
        {
            $this->$var = $valor;
        }
        else
        {
            throw new NotValidPropertyException("Response->" . $var);
        }
    }
    public function __get($var)
    {
        if (property_exists('Response', $var))
        {
            return $this->$var;
        }
        throw new NotValidPropertyException("Response->" . $var);
    }
    public function __toString()
    {
        return json_encode($this);
    }
    public function emitirJson()
    {
        echo $this;
    }
}
?>
