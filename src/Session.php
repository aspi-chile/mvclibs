<?php
/**
* Class and Function List:
* Function list:
* - init()
* - set()
* - get()
* - destroy()
* - checkLogin()
* Classes list:
* - Session
*/
class Session
  {
    public static function init()
      {
        @session_start();
        @ini_set('session.cookie_lifetime', 0);
      }
    public static function set($key, $value)
      {
        $_SESSION[$key] = $value;
      }
    public static function get($key)
      {
        if (isset($_SESSION[$key])) return $_SESSION[$key];
      }
    public static function destroy()
      {
        //unset($_SESSION);
        session_destroy();
      }
    public static function checkLogin()
      {
        if (Session::get('loggedIn')) return true;
        return false;
      }
  }
?>
