<?php
/**
* Class and Function List:
* Function list:
* - errorMessage()
* Classes list:
* - NotValidPropertyException extends Exception
*/
class NotValidPropertyException extends Exception
  {
    public function errorMessage()
      {
        $errorMsg = 'Error en linea ' . $this->getLine() . ' en ' . $this->getFile() . ': <b>' . $this->getMessage() . '</b> No es una propiedad valida.';
        return $errorMsg;
      }
  }
?>
