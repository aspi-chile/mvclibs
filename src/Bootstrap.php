<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - error()
* - validarConfiguracion()
* Classes list:
* - Bootstrap
*/
class Bootstrap
  {
    function __construct()
      {
        $this->loggerControlador = new \Monolog\Logger('loggerControlador');
        $this->loggerModelo = new \Monolog\Logger('loggerModelo');
        $this
            ->loggerControlador
            ->pushHandler(new \Monolog\Handler\StreamHandler('Controlador.log', \Monolog\Logger::DEBUG));
        $this
            ->loggerModelo
            ->pushHandler(new \Monolog\Handler\StreamHandler('Modelo.log', \Monolog\Logger::DEBUG));
        $this->validarConfiguracion();
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = ltrim($url, '/');
        $url = explode('/', $url);
        //sanitizar url
        if (empty($url[0]))
          {
            $controlador = new Index($this->loggerControlador);
            $controlador->cargarModelo('Index', $this->loggerModelo);
            $controlador->index();
            return false;
          }
        $url[0] = strtolower($url[0]);
        $url[0] = ucfirst($url[0]);
        //cambiar por class exits
        $fichero = 'controladores/' . $url[0] . '.php';
        if (!file_exists($fichero))
          {
            $this->error();
            return false;
          }
        $controlador = new $url[0]($this->loggerControlador);
        $controlador->cargarModelo($url[0], $this->loggerModelo);
        if (isset($url[3]))
          {
            if (method_exists($controlador, $url[1]))
              {
                $controlador->{$url[1]}($url[2], $url[3]);
              }
            else
              {
                $this->error();
              }
          }
        else
          {
            if (isset($url[2]))
              {
                if (method_exists($controlador, $url[1]))
                  {
                    $controlador->{$url[1]}($url[2]);
                  }
                else
                  {
                    $this->error();
                  }
              }
            else
              {
                if (isset($url[1]))
                  {
                    if (method_exists($controlador, $url[1]))
                      {
                        $controlador->{$url[1]}();
                      }
                    else
                      {
                        $this->error();
                      }
                  }
                else
                  {
                    $controlador->index();
                  }
              }
          }
      }
    function error()
      {
        $controlador = new Controlador($this->loggerControlador);
        $controlador->redirigirError(1005, array(
            'Pagina' => array(
                'No encontrada'
            )
        ));
        return false;
      }
    private function validarConfiguracion()
      {
        $arrayConstantesConfig = array(
            'NOMBRE_SISTEMA',
            'URL',
            'UPLOADS_PATH',
            'UPLOADS_PATH',
            'DB_TYPE',
            'DB_HOST',
            'DB_NAME',
            'DB_USER',
            'DB_PASS',
            'APP_PATH',
            'CONTROLADOR_PATH',
            'MODELOS_PATH',
            'VISTA_PATH'
        );
        foreach ($arrayConstantesConfig as $constante) if (!defined($constante))
          {
            echo "$constante no configurada";
            die;
          }
      }
  }
?>
