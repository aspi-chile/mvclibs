<?php
/**
* Class and Function List:
* Function list:
* - encriptar()
* Classes list:
* - Hash
*/
class Hash
  {
    /**
     *
     * @param string $algoritmo El Algoritmo a usar (md5, sha1, whirlpool,...).
     * @param string $dato Dato a encriptar.
     * @return string El $dato encriptado
     */
    public static function encriptar($algoritmo, $dato)
      {
        $cont = hash_init($algoritmo, HASH_HMAC);
        hash_update($cont, $dato);
        return hash_final($cont);
      }
  }
?>
