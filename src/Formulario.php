<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - set()
* - val()
* - checkCamposPost()
* - getError()
* - limpiarError()
* Classes list:
* - Formulario
*/
class Formulario
  {
    private $_itemActual = null;
    private $_postData = array();
    private $_error = array();
    function __construct()
      {
        $this->check = new Check();
        $this->_postData = array();
      }
    public function set($campo, $data)
      {
        $this->_postData[$campo] = $data;
        $this->_itemActual = $campo;
        return $this;
      }
    /**
     *
     * @param string $tipoValidacion
     *        rut, nombre, enteroPositivo, largoMaximo, largoMinimo, correo
     * @param int $arg
     *        tamanio para cantidad de caracteres u otra validacion
     * @return Form
     */
    public function val($tipoValidacion, $arg)
      {
        $resultado = $this
            ->check->{$tipoValidacion}($this->_postData[$this->_itemActual], $arg);
        if ($resultado)
          {
            $this->_error[$this->_itemActual][$tipoValidacion] = $resultado;
          }
        return $this;
      }
    public function checkCamposPost($campos = array())
      {
        foreach ($campos as $campo) if (!isset($_POST[$campo])) $this->_error[$campo]["Parametro Faltante"] = "Campo no existe";
        return $this;
      }
    public function getError()
      {
        // if(count($this->_error)>0){
        // return $this->_error;
        // }
        // return false;
        return $this->_error;
      }
    public function limpiarError()
      {
        $this->_error = array();
      }
  }
?>
