<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - contectarLocal()
* - conectarMemoria()
* - getInstancia()
* - destroyInstancia()
* - insertar()
* - actualizar()
* - eliminar()
* - seleccionar()
* - truncarTabla()
* - lastInsertedId()
* - limpiarDB()
* Classes list:
* - Database extends PDO
*/
class Database extends PDO
  {
    private static $instancia = NULL;
    public function __construct()
      {
      }
    public function contectarLocal($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
      {
        parent::__construct($DB_TYPE . ':host=' . $DB_HOST . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASS, array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        ));
      }
    public function conectarMemoria()
      {
        parent::__construct('sqlite::memory:');
      }
    public static function getInstancia($enMemoria = false)
      {
        if (Database::$instancia == NULL)
          {
            Database::$instancia = new Database();
            if (!$enMemoria) Database::$instancia->contectarLocal(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
            else Database::$instancia->conectarMemoria();
            Database::$instancia->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          }
        return Database::$instancia;
      }
    public static function destroyInstancia()
      {
        if (isset(Database::$instancia) && Database::$instancia != NULL) unset(Database::$instancia);
      }
    /**
     *
     * insertar
     *
     * @param string $tabla indica la tabla donde insertar datos
     * @param array $datos contiene los campos y la informacion a insertar
     */
    function insertar($tabla, $datos)
      {
        $nombreCampos = implode('`, `', array_keys($datos));
        $valores = ':' . implode(', :', array_keys($datos));
        $sql = "INSERT INTO $tabla (`$nombreCampos`) VALUES ($valores)";
        $sth = $this->prepare($sql);
        foreach ($datos as $llave => $valor)
          {
            $sth->bindValue(":$llave", $valor);
          }
        $sth->execute();
      }
    /**
     *
     * actualizar
     *
     * @param string $tabla indica la tabla donde insertar datos
     * @param array $datos contiene los campos y la informacion a insertar
     * @param string $condicion utilizada en la sentencia sql
     */
    function actualizar($tabla, $datos, $condicion)
      {
        $detalleCampos = NULL;
        foreach ($datos as $llave => $valor)
          {
            $detalleCampos .= "`$llave`=:$llave,";
          }
        $detalleCampos = rtrim($detalleCampos, ',');
        $sql = "UPDATE $tabla SET $detalleCampos WHERE $condicion";
        $sth = $this->prepare($sql);
        foreach ($datos as $llave => $valor)
          {
            $sth->bindValue(":$llave", $valor);
          }
        $sth->execute();
      }
    /**
     *
     * eliminar
     *
     * @param string $tabla a seleccionar
     * @param type $donde lugar a eliminar
     * @param type $limite condicion
     * @return type void
     */
    public function eliminar($tabla, $donde, $limite = 1)
      {
        return $this->exec("DELETE FROM $tabla WHERE $donde LIMIT $limite");
      }
    /**
     *
     * seleccionar
     *
     * @param string $sql sentencia sql
     * @param array $arreglo parametros a bindear
     * @param constant $modoFetch una forma PDO para fetch
     * @return type
     */
    public function seleccionar($sql, $arreglo = array(), $modoFetch = PDO::FETCH_ASSOC, $clase = null)
      {
        $sth = $this->prepare($sql);
        foreach ($arreglo as $llave => $valor)
          {
            $sth->bindValue("$llave", $valor);
          }
        $sth->execute();
        if($modoFetch == PDO::FETCH_CLASS){
        	return $sth->fetchAll($modoFetch, $clase);
        }
        return $sth->fetchAll($modoFetch);
      }
    public function truncarTabla($tabla)
      {
        $this->exec("TRUNCATE TABLE $tabla");
      }
    public function lastInsertedId()
      {
        return $this->lastInsertId();
      }
    public function limpiarDB()
      {
        $sth = $this->prepare("CALL sptruncate");
        $sth->execute();
      }
  }
?>
