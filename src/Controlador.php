<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - cargarModelo()
* - setModelo()
* - redirigirError()
* - __set()
* - __get()
* Classes list:
* - Controlador
*/
class Controlador
  {
    protected $_sanitizador;
    protected $_formulario;
    protected $_modelo;
    public $response;
    public $vista;
    protected $_logger;
    function __construct($logger, $vista = null)
      {
        $this->_logger = $logger;
        $this->_sanitizador = new Sanitizador();
        $this->_formulario = new Formulario();
        $this->response = new Response();
        Session::init();
        if ($vista != null) $this->vista = $vista;
        else $this->vista = new Vista();
      }
    public function cargarModelo($nombre, $logger)
      {
        $nombreModelo = $nombre . 'Modelo';
        $modelo = new $nombreModelo($logger);
        $this->setModelo($modelo);
      }
    public function setModelo($modelo)
      {
        $this->_modelo = $modelo;
      }
    public function redirigirError($codigo, $descripcion)
      {
        $this
            ->response
            ->error
            ->setErrorCode($codigo);
        $this
            ->vista
            ->bag['error'] = $this
            ->response->error;
        $this
            ->vista
            ->renderTwig('error');
        exit;
      }
    public function __set($var, $valor)
      {
        if (property_exists('Controlador', $var))
          {
            $this->$var = $valor;
          }
        else
          {
            throw new NotValidPropertyException("Controlador->" . $var);
          }
      }
    public function __get($var)
      {
        if (property_exists('Controlador', $var))
          {
            return $this->$var;
          }
        throw new NotValidPropertyException("Controlador->" . $var);
      }
  }
?>
