<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - setErrorCode()
* - __set()
* - __get()
* - __toString()
* Classes list:
* - ErrorResponse
*/
//codigos errores
define("ERROR_MSG_0", "Operación Exitosa");
define("ERROR_MSG_1000", "TimeOut");
define("ERROR_MSG_1001", "Parámetros Faltantes");
define("ERROR_MSG_1002", "Parámetro Formato Incorrecto");
define("ERROR_MSG_1003", "Acceso no autorizado");
define("ERROR_MSG_1004", "No se pudo subir el archivo");
define("ERROR_MSG_1005", "Página no encontrada");
define("ERROR_MSG_2000", "TimeOut");
define("ERROR_MSG_2001", "Parámetro No Válido");
define("ERROR_MSG_2002", "TimeOut");
define("ERROR_MSG_2003", "Conexión Invalida");
define("ERROR_MSG_2004", "No existe el registro");
define("ERROR_MSG_2005", "Acceso no valido");
define("ERROR_MSG_2006", "Información no encontrada");
define("ERROR_MSG_2007", "Error al insertar");
define("ERROR_MSG_2008", "Error al eliminar");
define("ERROR_MSG_2009", "Error al actualizar");
class ErrorResponse
{
    public $codigo;
    public $mensaje;
    public $descripcion;
    function __construct()
    {
        $this->setErrorCode(0);
        $this->descripcion = array();
    }
    public function setErrorCode($codigo)
    {
        $this->codigo = $codigo;
        if (defined("ERROR_MSG_" . $codigo))
            $this->mensaje = constant("ERROR_MSG_" . $codigo);
        else
            throw new NotValidPropertyException(
                "ErrorResponse->$codigo no definido");
    }
    public function __set($var, $valor)
    {
        if (property_exists('ErrorResponse', $var))
        {
            $this->$var = $valor;
        }
        else
        {
            throw new NotValidPropertyException("ErrorResponse->" . $var);
        }
    }
    public function __get($var)
    {
        if (property_exists('ErrorResponse', $var))
        {
            return $this->$var;
        }
        throw new NotValidPropertyException("ErrorResponse->" . $var);
    }
    public function __toString()
    {
        return json_encode($this);
    }
}
?>
