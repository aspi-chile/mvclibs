<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - __set()
* - __get()
* Classes list:
* - Modelo
*/
class Modelo
  {
    public $db;
    protected $_sanitizador;
    protected $_formulario;
    public $response;
    protected $_logger;
    function __construct($logger, $unitest = false)
      {
        $this->_logger = $logger;
        $this->db = Database::getInstancia($unitest);
        $this->_sanitizador = new Sanitizador();
        $this->_formulario = new Formulario();
        $this->response = new Response();
      }
    public function __set($var, $valor)
      {
        if (property_exists('Modelo', $var))
          {
            $this->$var = $valor;
          }
        else
          {
            throw new NotValidPropertyException("Modelo->" . $var);
          }
      }
    public function __get($var)
      {
        if (property_exists('Modelo', $var))
          {
            return $this->$var;
          }
        throw new NotValidPropertyException("Modelo->" . $var);
      }
  }
?>
