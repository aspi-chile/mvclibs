<?php
/**
* Class and Function List:
* Function list:
* - guardar()
* - crop()
* - image_resize()
* Classes list:
* - Fichero
*/
class Fichero
  {
    public static function guardar($fichero, $extension, $rutaBase, $urlBase)
      {
        $ruta = tempnam($rutaBase, date("YmdHis"));
        unlink($ruta);
        if (strpos($ruta, "/") !== false)
          {
            $words = explode("/", $ruta);
            $ruta = $words[count($words) - 1];
          }
        $ruta = $rutaBase . $ruta . "." . $extension;
        if (move_uploaded_file($fichero, $ruta))
          {
            return $urlBase . str_replace($rutaBase, '', $ruta);
          }
        return false;
      }
    public function crop($image, $x, $y, $width, $height)
      {
        try
          {
            return imagecrop($image, array(
                'x' => $x,
                'y' => $y,
                'width' => $width,
                'height' => $height
            ));
          }
        catch(Exception $e)
          {
            //print_r($e);
            
          }
      }
    static function image_resize($src, $dst, $width, $height, $ext, $crop = 0)
      {
        $ext = strtolower($ext);
        if (!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";
        if ($ext == 'jpeg') $ext = 'jpg';
        switch ($ext)
          {
        case 'bmp':
            $img = imagecreatefromwbmp($src);
        break;
        case 'gif':
            $img = imagecreatefromgif($src);
        break;
        case 'jpg':
            $img = imagecreatefromjpeg($src);
        break;
        case 'png':
            $img = imagecreatefrompng($src);
        break;
        default:
            return "Unsupported picture type!";
          }
        // resize
        /*if($crop){
        if($w < $width or $h < $height) return "Picture is too small!";
        $ratio = max($width/$w, $height/$h);
        $h = $height / $ratio;
        $x = ($w - $width / $ratio) / 2;
        $w = $width / $ratio;
        }
        else{
        if($w < $width and $h < $height) return "Picture is too small!";
        $ratio = min($width/$w, $height/$h);
        $width = $w * $ratio;
        $height = $h * $ratio;
        $x = 0;
        }*/
        $new = imagecreatetruecolor($width, $height);
        // preserve transparency
        if ($ext == "gif" or $ext == "png")
          {
            imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
            imagealphablending($new, false);
            imagesavealpha($new, true);
          }
        imagecopyresampled($new, $img, 0, 0, 0, 0, $width, $height, $w, $h);
        switch ($ext)
          {
        case 'bmp':
            imagewbmp($new, $dst);
        break;
        case 'gif':
            imagegif($new, $dst);
        break;
        case 'jpg':
            imagejpeg($new, $dst);
        break;
        case 'png':
            imagepng($new, $dst);
        break;
          }
        return true;
      }
  }
?>
