<?php
/**
* Class and Function List:
* Function list:
* - __construct()
* - renderTwig()
* - __set()
* - __get()
* Classes list:
* - Vista extends Twig_Environment
*/
class Vista extends Twig_Environment
  {
    protected $loader;
    public $bag;
    function __construct()
      {
        $this->loader = new Twig_Loader_Filesystem(VISTA_PATH);
        $this->bag = array(
            'URL' => URL
        );
        $this->bag['VISTA_PATH'] = VISTA_PATH;
        $this->bag['NOMBRE_SISTEMA'] = NOMBRE_SISTEMA;
        $this->bag['ERROR'] = array();
        parent::__construct($this->loader);
      }
    public function renderTwig($vista, $subVista = false, $incluirHeader = true, $incluirFooter = true, $incluirMenuPrincipal = true)
      {
        $this->bag['Session'] = $_SESSION;
        if ($incluirHeader == true) echo parent::render('header.php', $this->bag);
        if ($incluirMenuPrincipal == true) echo parent::render('menu.php', $this->bag);
        if ($subVista == false) echo parent::render($vista . '/index.php', $this->bag);
        else echo parent::render($vista . '/' . $subVista . '.php', $this->bag);
        if ($incluirFooter == true) echo parent::render('footer.php', $this->bag);
      }
    public function __set($var, $valor)
      {
        if (property_exists('Vista', $var))
          {
            $this->$var = $valor;
          }
        else
          {
            throw new NotValidPropertyException("Vista->" . $var);
          }
      }
    public function __get($var)
      {
        if (property_exists('Vista', $var))
          {
            return $this->$var;
          }
        throw new NotValidPropertyException("Vista->" . $var);
      }
  }
?>
